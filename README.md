# uf b3

Ce script sert a configurer et créer les services nécésaires au projet sur google cloud.

La variable PROJECT_ID est a modifier avec votre project id. 

• gcloud services enable 

Sert a activer l'api dans le projet pour pouvoir créer les ressources nécéssaires.

• gcloud compute 

Créer le réseaux vpc ainsi que ses sous-réseaux.

• gcloud beta container 

Créer les cluster GKE dans les zones spécifié.

• gcloud beta compute 

Créer des machines vituelles.